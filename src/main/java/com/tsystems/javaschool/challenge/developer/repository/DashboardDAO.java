package com.tsystems.javaschool.challenge.developer.repository;

import com.tsystems.javaschool.challenge.developer.domain.Story;

import java.util.List;

public interface DashboardDAO {

    boolean getStarted();
    List<Story> getAllStories();
    void update(Story story);
    Long getLastDeveloperID();
}
