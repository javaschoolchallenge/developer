package com.tsystems.javaschool.challenge.developer.service;

public interface StorySearchService {

    void checkAvailableStories();

}
