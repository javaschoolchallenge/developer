package com.tsystems.javaschool.challenge.developer.domain;

import lombok.Data;

@Data
public class Story {

    private long id;
    private Status status;
    private int estimation;
    private int priority;
    private int version;
    private Employee assigned;

    public void incrementVersion() {
        version++;
    }

    public enum Status {
        OPEN, IN_PROGRESS, RESOLVED, IN_TEST, CLOSED
    }

}
