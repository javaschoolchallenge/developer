package com.tsystems.javaschool.challenge.developer.service.impl;

import com.tsystems.javaschool.challenge.developer.domain.Employee;
import com.tsystems.javaschool.challenge.developer.domain.Story;
import com.tsystems.javaschool.challenge.developer.repository.DashboardDAO;
import com.tsystems.javaschool.challenge.developer.service.WorkService;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class WorkServiceImpl implements WorkService {

    @Value("${business.sp-multiplier}")
    private long spMultiplier;

    private final DashboardDAO dashboardDAO;

    @Override
    @SneakyThrows
    public void takeStoryForWork(Story story, Employee developer) {
        story.setStatus(Story.Status.IN_PROGRESS);
        story.setAssigned(developer);
        dashboardDAO.update(story);
        log.info("Took story for work: {}", story);
        log.info("Estimated work time: {}", spMultiplier * story.getEstimation());
        Thread.sleep(spMultiplier * story.getEstimation());
        story.setStatus(Story.Status.RESOLVED);
        story.setAssigned(null);
        dashboardDAO.update(story);
        log.info("Story {} is ready for testing!", story.getId());
    }

}
