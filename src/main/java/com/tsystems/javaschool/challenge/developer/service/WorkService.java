package com.tsystems.javaschool.challenge.developer.service;

import com.tsystems.javaschool.challenge.developer.domain.Employee;
import com.tsystems.javaschool.challenge.developer.domain.Story;

public interface WorkService {

    void takeStoryForWork(Story story, Employee developer);
}
