package com.tsystems.javaschool.challenge.developer.service.impl;

import com.tsystems.javaschool.challenge.developer.domain.Employee;
import com.tsystems.javaschool.challenge.developer.domain.Story;
import com.tsystems.javaschool.challenge.developer.repository.DashboardDAO;
import com.tsystems.javaschool.challenge.developer.service.StorySearchService;
import com.tsystems.javaschool.challenge.developer.service.WorkService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
@Slf4j
public class StorySearchServiceImpl implements StorySearchService {

    private final DashboardDAO dashboardDAO;
    private final WorkService workService;
    private Employee developer;

    @PostConstruct
    public void initBot() {
        try {
            Long id = dashboardDAO.getLastDeveloperID();
            developer = new Employee("Dev" + id, Employee.Position.DEVELOPER);
        } catch (Exception e) {
            developer = new Employee("Dev" + 1L, Employee.Position.DEVELOPER);
        }
        log.info("Developer {} was initialized.", developer.getName());
    }

    @Override
    @Scheduled(fixedDelay = 1000)
    public void checkAvailableStories() {
        log.warn("I am {}", developer.getName());
        if (!dashboardDAO.getStarted()) {
            log.info("Sprint is not started, waiting...");
            return;
        }
        List<Story> openStories = dashboardDAO.getAllStories()
                .stream()
                .filter(story -> story.getStatus() == Story.Status.OPEN)
                .sorted(Comparator.comparingInt(Story::getPriority))
                .collect(toList());
        log.info("Found {} open stories", openStories.size());
        log.warn("I am {}", developer.getName());
        if (!CollectionUtils.isEmpty(openStories)) {
            log.warn("Took story {}", openStories.get(0).getId());
            workService.takeStoryForWork(openStories.get(0), developer);
        } else {
            log.info( "There are no open stories.");
        }
    }

}
