package com.tsystems.javaschool.challenge.developer.repository.impl;

import com.tsystems.javaschool.challenge.developer.domain.Story;
import com.tsystems.javaschool.challenge.developer.exception.DashboardNotAvailable;
import com.tsystems.javaschool.challenge.developer.repository.DashboardDAO;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Repository
@Slf4j
public class DashboardDAOImpl implements DashboardDAO {

    @Value("${web.dashboard.url}")
    private String dashboardUrl;

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public boolean getStarted() {
        ResponseEntity<Boolean> response = restTemplate.getForEntity(dashboardUrl + "/sprint", Boolean.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            log.warn("Dashboard is not available!");
            return false;
        }
        return response.getBody();
    }

    @Override
    public List<Story> getAllStories() {
        log.info("Retrieving all stories from dashboard");
        ResponseEntity<Story[]> getResult = restTemplate.getForEntity(dashboardUrl + "/story", Story[].class);
        if (getResult.getStatusCode() != HttpStatus.OK) {
            throw new DashboardNotAvailable(getResult.toString());
        }
        List<Story> result = Arrays.asList(getResult.getBody());
        log.info("Retrieved {} stories", result.size());
        return result;
    }

    @Override
    public void update(Story story) {
        log.info("Updating story: {}", story);
        restTemplate.put(dashboardUrl + "/story/" + story.getId(), story);
        if (story.getVersion() != 0) {
            story.incrementVersion();
        }
    }

    @Override
    public Long getLastDeveloperID() {
        System.out.println("Fetching free developer");
        log.info("Fetching free developer");
        ResponseEntity<Long> response = restTemplate.getForEntity(dashboardUrl + "/developer", Long.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new DashboardNotAvailable();
        }
        return response.getBody();
    }
}
